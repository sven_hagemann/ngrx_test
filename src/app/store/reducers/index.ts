import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as fromTest from "./test.reducer";


export interface State {
  [fromTest.testFeatureKey]: fromTest.State,
}

export const reducers: ActionReducerMap<State> = {
  [fromTest.testFeatureKey]: fromTest.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];


/* #region Test */
export const selectTestState = createFeatureSelector<State, fromTest.State>(
  fromTest.testFeatureKey
);

export const selectTest = createSelector(
  selectTestState,
  fromTest.getTest
);
/* #endregion */