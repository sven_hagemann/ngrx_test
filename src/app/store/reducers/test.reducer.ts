import { Action, createReducer, on } from '@ngrx/store';
import { setTest } from '../actions/test.actions';


export const testFeatureKey = 'test';

export interface State {
  test: any;
}

export const initialState: State = {
  test: null,
};


export const reducer = createReducer(
  initialState,
  on(
    setTest, (state, { data }) => ({
      ...state,
      test: data,
    })),
);

export const getTest = (state: State) => state.test;