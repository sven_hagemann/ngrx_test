import { createAction, props } from '@ngrx/store';

export const loadTest = createAction(
  '[Test] Load Tests'
);

export const loadTestSuccess = createAction(
  '[Test] Load Tests Success',
  props<{ data: any }>()
);

export const loadTestFailure = createAction(
  '[Test] Load Tests Failure',
  props<{ error: any }>()
);

export const setTest = createAction(
  '[Test] Set Test',
  props<{ data: any }>()
);